# mehmet-kozan

**The internal test of my npm packages...**

[![version](https://img.shields.io/npm/v/mehmet-kozan.svg)](https://www.npmjs.org/package/mehmet-kozan)
[![downloads](https://img.shields.io/npm/dt/mehmet-kozan.svg)](https://www.npmjs.org/package/mehmet-kozan)
[![node](https://img.shields.io/node/v/mehmet-kozan.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/mehmet-kozan/badges/master/pipeline.svg)](https://gitlab.com/autokent/mehmet-kozan/pipelines)
[![WhatsApp](https://img.shields.io/badge/style-chat-green.svg?style=flat&label=whatsapp)](https://api.whatsapp.com/send?phone=905063042480&text=Hello)
## Installation
`npm install mehmet-kozan`

## Test
`mocha` or `npm test`

Check [test folder](https://gitlab.com/autokent/mehmet-kozan/tree/master/test) and [quickstart.js](https://gitlab.com/autokent/mehmet-kozan/blob/master/quickstart.js) for extra usages.

